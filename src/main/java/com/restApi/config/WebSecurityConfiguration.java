package com.restApi.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        System.out.println("Used");
        http.csrf().disable().authorizeRequests().antMatchers("/*").permitAll().
                antMatchers(HttpMethod.POST, "/request1").permitAll()
                .antMatchers(HttpMethod.GET, "/request").permitAll()
                .anyRequest().authenticated();
    }
}
