package com.restApi.controller;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

@RestController
public class MainController {

    @RequestMapping(value = "/request1",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String req(HttpServletRequest request, InputStream incomingData) throws Exception{
        StringBuilder inputData=new StringBuilder();
        BufferedReader in =new BufferedReader(new InputStreamReader(incomingData));

        String line=null;
        while((line=in.readLine())!=null){
            inputData.append(line);
        }

        JSONParser jsonParser=new JSONParser();
        JSONObject jsonObject=(JSONObject) jsonParser.parse(inputData.toString());
        return jsonObject.toJSONString();
    }

    @RequestMapping(value = "/request",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String req(HttpServletRequest request) throws Exception{
        return "abc";
    }
}
